const AWS = require('aws-sdk')
const dynamo = new AWS.DynamoDB.DocumentClient();
exports.handler = async (event) => {
    // TODO implement
    const item = JSON.parse(event.body);
    
    if(item.id === '-1' || !item.id){
        item.id = Math.random() * Math.pow(10,16) + '';
    }
    
    var params = {
     TableName : process.env.TODO_TABLE, 
     Item: item
    };
    
   const result = await dynamo.put(params).promise();
   const statusCode = 200;
   const headers = {'Access-Control-Allow-Origin': '*'}
   const response = {statusCode, body:'', headers};
   
    return response;
};